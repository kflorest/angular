// Imports for loading & configuring the in-memory web api
import { XHRBackend }           from '@angular/http';
import { InMemoryBackendService,
        SEED_DATA }             from 'angular2-in-memory-web-api';
import { InMemoryDataService }  from './app/service/in-memory-data.service';

// The usual bootstrapping imports
import { bootstrap }      from '@angular/platform-browser-dynamic';
import { HTTP_PROVIDERS } from '@angular/http';
import { enableProdMode } from '@angular/core';

import { AppComponent }   		from './app/app.component';
import { APP_ROUTER_PROVIDERS }	from './app/router/app.routes';

if (process.env.ENV === 'production') {
  enableProdMode();
}
bootstrap(AppComponent, [
  APP_ROUTER_PROVIDERS,
  HTTP_PROVIDERS,
  { provide: XHRBackend, useClass: InMemoryBackendService },
  { provide: SEED_DATA, useClass: InMemoryDataService }
]).catch(err => console.log(err));
