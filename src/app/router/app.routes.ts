import { provideRouter, RouterConfig  }	from '@angular/router';
import { HeroesComponent }      from './../components/heroes.component';
import { DashboardComponent }   from './../components/dashboard.component';
import { HeroDetailComponent }  from './../components/hero-detail.component';
import { WikiComponent }  from './../wiki/wiki.component';
import { WikiSmartComponent }  from './../wiki/wiki-smart.component';

export const routes: RouterConfig  = [
    { path: 'heroes', component: HeroesComponent },
    { path: 'dashboard', component: DashboardComponent, pathMatch: 'full' },
    { path: 'wiki', component: WikiComponent },
    { path: 'wikismart', component: WikiSmartComponent },
    { path: 'detail/:id', component: HeroDetailComponent }
];

export const APP_ROUTER_PROVIDERS = [
	provideRouter(routes)
];