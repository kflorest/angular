"use strict";
var router_1 = require('@angular/router');
var heroes_component_1 = require('./../components/heroes.component');
var dashboard_component_1 = require('./../components/dashboard.component');
var hero_detail_component_1 = require('./../components/hero-detail.component');
var wiki_component_1 = require('./../wiki/wiki.component');
var wiki_smart_component_1 = require('./../wiki/wiki-smart.component');
exports.routes = [
    { path: 'heroes', component: heroes_component_1.HeroesComponent },
    { path: 'dashboard', component: dashboard_component_1.DashboardComponent, pathMatch: 'full' },
    { path: 'wiki', component: wiki_component_1.WikiComponent },
    { path: 'wikismart', component: wiki_smart_component_1.WikiSmartComponent },
    { path: 'detail/:id', component: hero_detail_component_1.HeroDetailComponent }
];
exports.APP_ROUTER_PROVIDERS = [
    router_1.provideRouter(exports.routes)
];
//# sourceMappingURL=app.routes.js.map