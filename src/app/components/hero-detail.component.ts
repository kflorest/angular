import {
    Component, EventEmitter,
    Input, Output,
    OnInit, OnDestroy } from '@angular/core';
import {
    Router,
    ActivatedRoute }    from '@angular/router';
import { Hero }         from './../classes/hero';
import { HeroService }  from './../service/hero.service';

@Component({
  selector: 'my-hero-detail',
  template: require('./templates/hero-detail.component.html'),
  styles: [require('./styles/hero-detail.component.css')]
})
export class HeroDetailComponent implements OnInit, OnDestroy {
  @Input() hero: Hero;
  @Output() close = new EventEmitter();
  error: any;
  private navigated = false; // true if navigated here
  private sub: any;
  constructor(
    private heroService: HeroService,
    private route: ActivatedRoute,
    private router: Router) {
  }
  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      if (params['id'] !== undefined) {
        let id = +params['id'];
        this.navigated = true;
        this.heroService.getHero(id)
            .then(hero => this.hero = hero);
      } else {
        this.navigated = false;
        this.hero = new Hero();
      }
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  save() {
    this.heroService
        .save(this.hero)
        .then(hero => {
          this.hero = hero; // saved hero, w/ id if new
          this.goBack(hero);
        })
        .catch(error => this.error = error); // TODO: Display error message
  }
  goBack(savedHero: Hero = null) {
    this.close.emit(savedHero);
    if (this.navigated) { window.history.back() }
  }
}
