import { Component, OnInit}     from '@angular/core';
import { Router }               from '@angular/router';

import { Hero }                 from './../classes/hero';
import { HeroService }          from './../service/hero.service';

import { HeroDetailComponent }  from './hero-detail.component';
import '../../../public/css/style.css';

@Component({
  selector: 'my-heroes',
  template: require('./templates/heroes.component.html'),
  styles: [require('./styles/heroes.component.css')],
  directives: [HeroDetailComponent]
})
export class HeroesComponent implements OnInit {
  title = 'Tour of Heroes';
  heroes: Hero[];
  selectedHero: Hero;
  addingHero = false;
  error: any;
  constructor(
    private router: Router,
    private heroService: HeroService
  ) {}
  getHeroes() {
    this.heroService.getHeroes()
        .then(heroes => this.heroes = heroes,
              error => this.error = <any>error);
  }
  ngOnInit() {
    this.getHeroes();
  }
  onSelect(hero: Hero) { this.selectedHero = hero; }
  gotoDetail(){
    this.router.navigate(['/detail', this.selectedHero.id]);
  }
  addHero() {
    this.addingHero = true;
    this.selectedHero = null;
  }
  close(savedHero: Hero) {
    this.addingHero = false;
    if (savedHero) { this.getHeroes(); }
  }
  delete(hero: Hero, event: any) {
    event.stopPropagation();
    this.heroService
        .delete(hero)
        .then(res => {
          this.heroes = this.heroes.filter(h => h !== hero);
          if (this.selectedHero === hero) { this.selectedHero = null; }
        })
        .catch(error => this.error = error); // TODO: Display error message
  }
}
