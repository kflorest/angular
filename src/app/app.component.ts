import { Component }            from '@angular/core';
import { ROUTER_DIRECTIVES }    from '@angular/router';

import './rxjs-operators'; // rxjs for all components
import { HeroService }          from './service/hero.service';

@Component({
  selector: 'my-app',
  template: require('./app.component.html'),
  styles: [require('./app.component.css')],
  directives: [ROUTER_DIRECTIVES],
  providers: [
    HeroService
  ]
})
export class AppComponent {
  title = 'Tour of Heroes';
}
